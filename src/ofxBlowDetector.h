//
//  ofxBlowDetector.h
//  ColdWindow
//
//  Created by Paul Wand on 6/9/12.
//  Copyright (c) 2012 RGB. All rights reserved.
//

#ifndef ColdWindow_ofxBlowDetector_h
#define ColdWindow_ofxBlowDetector_h
#include "ofMain.h"
class ofxBlowDetector{
public:
    string message;
    void setup(){
        
    };
    void update(){
        //scale to number between 0 and 1
    	scaledVol = ofMap(smoothedVol, 0.0, 0.17, 0.0, 1.0, true);
    	//lets record the volume into an array
        volHistory.push_back( scaledVol );
        
        //if we are bigger the the size we want to record - lets drop the oldest value
        if( volHistory.size() >= 400 ){
            volHistory.erase(volHistory.begin(), volHistory.begin()+1);
        }
        if(smoothedVol*40>.02){
            vol.x=volume;
            vol.y=smoothedVol;
            ofNotifyEvent(blowDetected,vol,this);
        }

}
    void draw(){
    	ofPushStyle();
		ofPushMatrix();
		ofTranslate(20, 20, 0);
        
		ofSetColor(225);
		ofDrawBitmapString(message+"Scaled average vol (0-100): " + ofToString(volume*200), 4, 18);
		ofCircle(200, 200, smoothedVol * 300.0f);
		
		ofBeginShape();
		for (int i = 0; i < volHistory.size(); i++){
			if( i == 0 ) ofVertex(i, 400);
            
			ofVertex(i, 400 - volHistory[i] * 70);
			
			if( i == volHistory.size() -1 ) ofVertex(i, 400);
		}
		ofEndShape(false);		
        
		ofPopMatrix();
        ofPopStyle();
        

    }
    float scaledVol=0;
    ofVec2f vol;
    float volume;
    float smoothedVol=0;
    vector <float> volHistory;

    ofEvent<ofVec2f> blowDetected;
    void audioIn(float *input, int bufferSize, int nChannels){
        float total;
        for (int i = 0; i < bufferSize; i++){
            total+=input[i];
        }
        volume=total/bufferSize;
        smoothedVol*=.93;
        smoothedVol+=volume*.7;

    }

};

#endif
