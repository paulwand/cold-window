//
//  ButtonController.h
//  ColdWindow
//
//  Created by Paul Wand on 6/12/12.
//  Copyright (c) 2012 RGB. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "testApp.h"

typedef void (testApp::*callbackMethod) (void );

@interface ButtonController : UIViewController{
@public
    IBOutlet UIButton* button;

    callbackMethod callback;
}
-(IBAction)buttonDown:(UIButton * )sender;
-(void)setCallback:(callbackMethod)method;
-(UIButton*)instance;
@end
