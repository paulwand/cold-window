//
//  ofxParticle.h
//  Particles
//
//  Created by Paul Wand on 5/24/12.
//  Copyright (c) 2012 RGB. All rights reserved.
//
//  A simple Particle class
//
//
#pragma once
#include "ofMath.h"
#include "ofMain.h"
class ofxParticle {
public:
	
        // constructors
	ofxParticle();
        // methods
	void update();
	void addForce(ofVec2f force);
	void draw();
	void checkEdges();	
    // properties
    ofVec2f loc;
	ofVec2f vel;
	ofVec2f accel;
    ofVec2f prev;
    bool immortal;
	float x=loc.x;
    
    float friction=.9;
	float mass=1;
	float topSpeed;
	float radius=1;
    float life=1;
};