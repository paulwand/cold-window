//
//  ofxColdWindow.cpp
//  Particles
//
//  Created by Paul Wand on 6/2/12.
//  Copyright (c) 2012 RGB. All rights reserved.
//
#include "testApp.h"
#include "ofxColdWindow.h"

void ofxColdWindow::setup(){

    particles.setup();
    foreground.setup();
    foreground.setChillyness(cold);
    foreground.setWindowType(plain);
    message="";
    blowDetector.setup();
    ofAddListener(particles.particleDeath, this, &ofxColdWindow::particleDeath);
    ofAddListener(blowDetector.blowDetected, this, &ofxColdWindow::blowDetected);
    background.setup();
}
void ofxColdWindow::draw(){
    background.draw();
    foreground.draw();
    particles.draw();
    //    blowDetector.draw();
    ofDrawBitmapString(message, 100, 100);
}
void ofxColdWindow::update(){
    foreground.update();
    blowDetector.update();
    background.update();
    particles.update();

    //    message="fps:"+ofToString(ofGetFrameRate())+" \n moving particles: "+ofToString(particles.numParticles())+"\n inert particles:"+ofToString(particles.inertParticles.size());
    for ( auto &well:particles.wells) {
        if(well.active){
            
            foreground.pixels.eraseSoftCircle(well.location.x, well.location.y, well.radius, 0);
            auto middle=well.location.middle(well.pLocation);
            auto q1=middle.middle(well.location);
            auto q2=middle.middle(well.pLocation);
            foreground.pixels.eraseSoftCircle(middle.x, middle.y, well.radius, 0);
            foreground.pixels.eraseSoftCircle(q1.x, q1.y, well.radius,0);
            foreground.pixels.eraseSoftCircle(q2.x, q2.y, well.radius,0);
            well.update();
            //  pixels.eraseLine(well.location.x, well.location.y, well.pLocation.x, well.pLocation.y, 0);            
        }
    }
}

//--------------------------------------------------------------
void ofxColdWindow::touchDown(ofTouchEventArgs &touch){
    particles.activateGravityWell(touch.id,touch.x,touch.y);
    particles.addParticles(touch.x, touch.y, 40);
}

//--------------------------------------------------------------
void ofxColdWindow::touchMoved(ofTouchEventArgs &touch){
    particles.moveGravityWell(touch.x, touch.y, touch.id);
        particles.addParticles(touch.x+particles.wells[touch.id].vel.x, touch.y+particles.wells[touch.id].vel.y, 40);
}

//--------------------------------------------------------------
void ofxColdWindow::touchUp(ofTouchEventArgs &touch){
    particles.removeGravityWell(touch.id);
}

void ofxColdWindow::particleDeath(ofVec2f & args){
//    foreground.pixels.setPixel(args.x, args.y);
}
void ofxColdWindow::blowDetected(ofVec2f &pwr){
  //  foreground.pixels.fadeToDefault(4);
}

void ofxColdWindow::reset(){
    foreground.reset();
}
