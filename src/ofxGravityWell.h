//
//  ofxGravityWell.h
//  Particles
//
//  Created by Paul Wand on 5/26/12.
//  Copyright (c) 2012 RGB. All rights reserved.
//
//  A simple Gravity well class
//
//

#ifndef Particles_ofxGravityWell_h
#define Particles_ofxGravityWell_h
#include "ofMain.h"
#include "ofxParticle.h"

class ofxGravityWell{
public:
    int id;
    float mass;
    ofVec2f location;
    ofVec2f pLocation;
    ofVec2f vel;
    bool active=false;
    ofVec2f destination;
    void gravitate(ofxParticle *particle);
    void update();
    float radius;
    float force;
    void draw();    
private:
    ofVec2f frc;
};


#endif
