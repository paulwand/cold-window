//
//  BackgroundChooser.h
//  ColdWindow
//
//  Created by Paul Wand on 6/12/12.
//  Copyright (c) 2012 RGB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ofMain.h"
#import "testApp.h"

@interface BackgroundChooser : UIViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate>{
    testApp *myApp;
}

-(IBAction)setLiveVideo:(UIButton * )sender;
-(IBAction)choosePicture:(UIButton * )sender;

-(IBAction)setPicture1:(UIButton * )sender;
-(IBAction)setPicture2:(UIButton * )sender;
-(IBAction)setPicture3:(UIButton * )sender;
-(IBAction)setPicture4:(UIButton * )sender;

-(IBAction)setVideo1:(UIButton * )sender;
-(IBAction)setVideo2:(UIButton * )sender;
-(IBAction)setVideo3:(UIButton * )sender;
-(IBAction)setVideo4:(UIButton * )sender;
-(void)done;

@end
