//
//  BackgroundChooser.m
//  ColdWindow
//
//  Created by Paul Wand on 6/12/12.
//  Copyright (c) 2012 RGB. All rights reserved.
//

#import "BackgroundChooser.h"
#import "imagehelper.h"
#import "UIImage+Resize.h"

@interface BackgroundChooser ()

@end

@implementation BackgroundChooser

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        myApp=(testApp*)ofGetAppPtr();
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}






-(IBAction)setLiveVideo:(UIButton * )sender{
    myApp->window.background.setMode(live);
    [self done];
}
-(IBAction)choosePicture:(UIButton * )sender{
    myApp->window.background.setMode(img);
    auto imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.allowsImageEditing = YES;
    imagePicker.delegate=self;
    [self presentModalViewController:imagePicker animated:YES];
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
	// Access the uncropped image from info dictionary
	UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    auto resizedImage=[image resizedImage:CGSizeMake(ofGetWidth(), ofGetHeight()) interpolationQuality:kCGInterpolationHigh];
    myApp->window.background.current->setFromPixels([ImageHelper convertUIImageToBitmapRGBA8:resizedImage], ofGetWidth(), ofGetHeight(), OF_IMAGE_COLOR_ALPHA);
 	// Save image
    self.view.hidden=YES;
    [picker dismissModalViewControllerAnimated:YES];

}


-(IBAction)setPicture1:(UIButton * )sender{
    myApp->window.background.setMode(img);
    myApp->window.background.current->loadImage("image01.jpg");
    [self done];
}
-(IBAction)setPicture2:(UIButton * )sender{
    myApp->window.background.setMode(img);
    myApp->window.background.current->loadImage("image02.jpg");
    [self done];
}
-(IBAction)setPicture3:(UIButton * )sender{
    myApp->window.background.setMode(img);
    myApp->window.background.current->loadImage("image03.jpg");
    [self done];
}
-(IBAction)setPicture4:(UIButton * )sender{
    myApp->window.background.current->loadImage("image04.jpg");
    myApp->window.background.setMode(img);
    [self done];
}

-(IBAction)setVideo1:(UIButton * )sender{
    myApp->window.background.video.loadMovie("video01.m4v");
    myApp->window.background.setMode(vid);
    [self done];
}
-(IBAction)setVideo2:(UIButton * )sender{
    myApp->window.background.video.loadMovie("video01.m4v");
    myApp->window.background.setMode(vid);
    [self done];
}
-(IBAction)setVideo3:(UIButton * )sender{
    myApp->window.background.video.loadMovie("video01.m4v");
    myApp->window.background.setMode(vid);
    [self done];
}
-(IBAction)setVideo4:(UIButton * )sender{
    myApp->window.background.video.loadMovie("video01.m4v");
    myApp->window.background.setMode(vid);
    [self done];
}


-(void)done{
    [UIView transitionWithView:ofxiPhoneGetUIWindow() duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve                     animations:^ { [self.view removeFromSuperview]; }
                    completion:nil];

}
@end
