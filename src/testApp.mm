#include "testApp.h"
#include "ButtonController.h"
#include "BackgroundChooser.h"
#include "ForegroundChooser.h"
//--------------------------------------------------------------
void testApp::setup(){	
	// register touch events
	ofRegisterTouchEvents(this);

	// initialize the accelerometer
	ofxAccelerometer.setup();
	
	//iPhoneAlerts will be sent to this.
	ofxiPhoneAlerts.addListener(this);
	
    ofSetFrameRate(60);
	//If you want a landscape oreintation 
	//iPhoneSetOrientation(OFXIPHONE_ORIENTATION_LANDSCAPE_RIGHT);
    
    width=ofxiPhoneGetOFWindow()->getWidth();
    height=ofxiPhoneGetOFWindow()->getHeight();
    ofEnableAlphaBlending();
	ofBackground(127,127,127);
    
    window.setup();
    ofSoundStreamSetup(0, 1);    
    
    auto recordButton=[[ButtonController alloc] initWithNibName:@"ButtonController" bundle:nil];
    auto backgroundButton=[[ButtonController alloc] initWithNibName:@"ButtonController" bundle:nil];
    auto shareButton=[[ButtonController alloc] initWithNibName:@"ButtonController" bundle:nil];
    auto foregroundButton=[[ButtonController alloc] initWithNibName:@"ButtonController" bundle:nil];
    int size=50;
    recordButton.view.frame = CGRectMake(0, 0, size, size);  
    foregroundButton.view.frame = CGRectMake(width-size, 0, width, size);  
    backgroundButton.view.frame = CGRectMake(0, height-size, size, height);  
    shareButton.view.frame = CGRectMake(width-size, height-size, width, height);  
    [recordButton setCallback:&testApp::record];
    [foregroundButton setCallback:&testApp::chooseForeground];
    [backgroundButton setCallback:&testApp::chooseBackground];
    [shareButton setCallback:&testApp::share];
    recordButton.instance.titleLabel.text=@"R" ; 
    backgroundButton.instance.titleLabel.text=@"B" ; 
    foregroundButton.instance.titleLabel.text=@"F" ; 
    shareButton.instance.titleLabel.text=@"S"; 
    [ofxiPhoneGetUIWindow() addSubview:shareButton.view];  
    [ofxiPhoneGetUIWindow() addSubview:recordButton.view];
	[ofxiPhoneGetUIWindow() addSubview:foregroundButton.view];  
    [ofxiPhoneGetUIWindow() addSubview:backgroundButton.view];

}

//--------------------------------------------------------------
void testApp::update(){
    window.update();
}

//--------------------------------------------------------------
void testApp::draw(){
    window.draw();

}

//--------------------------------------------------------------
void testApp::exit(){

}

//--------------------------------------------------------------
void testApp::touchDown(ofTouchEventArgs &touch){
    window.touchDown(touch);    

}

//--------------------------------------------------------------
void testApp::touchMoved(ofTouchEventArgs &touch){
    window.touchMoved(touch);
}

//--------------------------------------------------------------
void testApp::touchUp(ofTouchEventArgs &touch){
    window.touchUp(touch);
}

//--------------------------------------------------------------
void testApp::touchDoubleTap(ofTouchEventArgs &touch){

}

//--------------------------------------------------------------
void testApp::lostFocus(){

}

//--------------------------------------------------------------
void testApp::gotFocus(){

}

//--------------------------------------------------------------
void testApp::gotMemoryWarning(){

}

//--------------------------------------------------------------
void testApp::deviceOrientationChanged(int newOrientation){

}


//--------------------------------------------------------------
void testApp::touchCancelled(ofTouchEventArgs& args){

}
//--------------------------------------------------------------
void testApp::audioIn(float *input, int bufferSize, int nChannels){
    window.blowDetector.audioIn(input, bufferSize, nChannels);
}


#pragma mark UI
void testApp::record(){
    window.message="record";
};
void testApp::share(){
    window.message="share";
};
void testApp::chooseBackground(){
    auto bgPicker=[[BackgroundChooser alloc] initWithNibName:@"BackgroundChooser" bundle:nil];
    [UIView transitionWithView:ofxiPhoneGetUIWindow() duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^ { [ofxiPhoneGetUIWindow() addSubview:bgPicker.view]; }
                    completion:nil];

};
void testApp::chooseForeground(){
    auto fgPicker=[[ForegroundChooser alloc] initWithNibName:@"ForegroundChooser" bundle:nil];
    [UIView transitionWithView:ofxiPhoneGetUIWindow() duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^ { [ofxiPhoneGetUIWindow() addSubview:fgPicker.view]; }
                    completion:nil];
};


