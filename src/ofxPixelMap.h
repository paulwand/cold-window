//
//  ofxPixelMap.h
//  Particles
//
//  Created by Paul Wand on 5/27/12.
//  Copyright (c) 2012 RGB. All rights reserved.
//
//
//
//  This wraps an oftexture with an unsigned char array
//  Useful for speedy texture bitmap things
//
//

#ifndef Particles_ofxPixelMap_h
#define Particles_ofxPixelMap_h
#include "ofMain.h"



class ofxPixelMap {
public:
    unsigned char 	* pixels;
    unsigned char 	* defaultPixels;

    int width;
    int height;
    void setup();
    
    void draw();
    void update();

    ofColor color;
    ofColor bgColor;
    void setPixel(int x,int y);
    void setPixelAlpha(int x,int y,int a);
    void removePixel(int x,int y);
    
    void drawCircle(int x,int y,int radius);
    void drawFillCircle(int x,int y,int radius);

    void eraseCircle(int x,int y,int radius,int a);
    void eraseFillCircle(int x,int y,int radius,int a);
    void eraseSoftCircle(int x,int y,int radius,int a);
    void eraseSoftLine(ofVec2f origin,ofVec2f dest,int radius);
    
    void drawLine(int x0,int y0,int x1,int y1);
    void eraseLine(int x0,int y0,int x1,int y1,int a);
    
    void drawSnowFlake(int x,int y,int a);
    void eraseSnowFlake(int x,int y,int a);
    
    void saveDefault();
    void fadeToDefault(int qty);
        
    
    void clear();
    
    ofTexture view;

};

#endif
