//
//  ButtonController.m
//  ColdWindow
//
//  Created by Paul Wand on 6/12/12.
//  Copyright (c) 2012 RGB. All rights reserved.
//

#import "ButtonController.h"

@interface ButtonController ()

@end

@implementation ButtonController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}
- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
        
    return NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(IBAction)buttonDown:(UIButton *)sender{
    testApp *myApp=(testApp*)ofGetAppPtr();
    (myApp->*callback)();
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
- (void)setCallback:(callbackMethod)method{
    callback=method;
}

-(UIButton*)instance{
    return button;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
