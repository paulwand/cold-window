//
//  ofxPixelMap.h
//  Particles
//
//  Created by Paul Wand on 5/27/12.
//  Copyright (c) 2012 RGB. All rights reserved.
//


#include <iostream>
#include "ofxPixelMap.h"

void ofxPixelMap::setup(){
    width=ofGetWidth();
    height=ofGetHeight();
    view.allocate(width, height, GL_RGBA);
	pixels = new unsigned char [height*width*6];
    defaultPixels = new unsigned char [height*width*6];
    pixels+=height*width;
    defaultPixels+=height*width;
    bgColor.set(0, 0, 0,0);
    color.set(250, 250, 255,80);
}
void ofxPixelMap::update(){
	view.loadData(pixels, width,height, GL_RGBA);
}
void ofxPixelMap::setPixel(int x, int y){
    pixels[(y*width+x)*4 + 0] += color.r;	// r
    pixels[(y*width+x)*4 + 1] += color.g;	// g
    pixels[(y*width+x)*4 + 2] += color.b;	// b
    pixels[(y*width+x)*4 + 3] += color.a;	// a
}
void ofxPixelMap::removePixel(int x, int y){
    pixels[((y%height)*width+x%width)*4 + 0] -= color.r;	// r
    pixels[((y%height)*width+x%width)*4 + 1] -= color.g;	// g
    pixels[((y%height)*width+x%width)*4 + 2] -= color.b;	// b
    pixels[((y%height)*width+x%width)*4 + 3] -= color.a;	// a
}

void ofxPixelMap::eraseCircle(int x0, int y0, int radius,int a){
    int f = 1 - radius;
    int ddF_x = 1;
    int ddF_y = -2 * radius;
    int x = 0;
    int y = radius;
    
    setPixelAlpha(x0, y0 + radius,a);
    setPixelAlpha(x0, y0 - radius,a);
    setPixelAlpha(x0 + radius, y0,a);
    setPixelAlpha(x0 - radius, y0,a);
    
    while(x < y)
    {
        // ddF_x == 2 * x + 1;
        // ddF_y == -2 * y;
        // f == x*x + y*y - radius*radius + 2*x - y + 1;
        if(f >= 0) 
        {
            y--;
            ddF_y += 2;
            f += ddF_y;
        }
        x++;
        ddF_x += 2;
        f += ddF_x;    
        setPixelAlpha(x0 + x, y0 + y,a);
        setPixelAlpha(x0 - x, y0 + y,a);
        setPixelAlpha(x0 + x, y0 - y,a);
        setPixelAlpha(x0 - x, y0 - y,a);
        setPixelAlpha(x0 + y, y0 + x,a);
        setPixelAlpha(x0 - y, y0 + x,a);
        setPixelAlpha(x0 + y, y0 - x,a);
        setPixelAlpha(x0 - y, y0 - x,a);
    }
}

void ofxPixelMap::eraseSoftCircle(int x0, int y0, int radius, int a){
    eraseFillCircle(x0, y0, radius+5,200);
    eraseFillCircle(x0, y0, radius+3,150);
    eraseFillCircle(x0, y0, radius,100);
    eraseFillCircle(x0, y0, radius-2,80);
    eraseFillCircle(x0, y0, radius-4,60);
    eraseFillCircle(x0, y0, radius-8,30);
    eraseFillCircle(x0, y0, radius-10,0);
}

void ofxPixelMap::eraseSoftLine(ofVec2f origin, ofVec2f dest, int radius){

}

void ofxPixelMap::eraseFillCircle(int x0,int y0,int radius,int a){
    int f = 1 - radius;
    int ddF_x = 1;
    int ddF_y = -2 * radius;
    int x = 0;
    int y = radius;
    
    setPixelAlpha(x0, y0 + radius,a);
    setPixelAlpha(x0, y0 - radius,a);
    eraseLine(x0 - radius, y0,x0 + radius, y0,a);

    
    while(x < y)
    {
        if(f >= 0) 
        {
            y--;
            ddF_y += 2;
            f += ddF_y;
        }
        x++;
        ddF_x += 2;
        f += ddF_x;      
        eraseLine(x0 - x, y0 + y,x0 + x, y0 + y,a);
        eraseLine(x0 - x, y0 - y,x0 + x, y0 - y,a);
        eraseLine(x0 - y, y0 + x,x0 + y, y0 + x,a);
        eraseLine(x0 - y, y0 - x,x0 + y, y0 - x,a);
    }
}
void ofxPixelMap::drawFillCircle(int x0, int y0,int radius){
        int f = 1 - radius;
        int ddF_x = 1;
        int ddF_y = -2 * radius;
        int x = 0;
        int y = radius;
       
        setPixel(x0, y0 + radius);
        setPixel(x0, y0 - radius);
        drawLine(x0 - radius, y0,x0 + radius, y0);

        while(x < y)
        {
            if(f >= 0) 
            {
                y--;
                ddF_y += 2;
                f += ddF_y;
            }
            x++;
            ddF_x += 2;
            f += ddF_x;    
            drawLine(x0 - x, y0 + y,x0 + x, y0 + y);
            drawLine(x0 - x, y0 - y,x0 + x, y0 - y);
            drawLine(x0 - y, y0 + x,x0 + y, y0 + x);
            drawLine(x0 - y, y0 - x,x0 + y, y0 - x);
        }
    }

void ofxPixelMap::drawCircle(int x0, int y0,int radius){
    int f = 1 - radius;
    int ddF_x = 1;
    int ddF_y = -2 * radius;
    int x = 0;
    int y = radius;
    
    setPixel(x0, y0 + radius);
    setPixel(x0, y0 - radius);
    setPixel(x0 + radius, y0);
    setPixel(x0 - radius, y0);
    
    while(x < y)
    {
        // ddF_x == 2 * x + 1;
        // ddF_y == -2 * y;
        // f == x*x + y*y - radius*radius + 2*x - y + 1;
        if(f >= 0) 
        {
            y--;
            ddF_y += 2;
            f += ddF_y;
        }
        x++;
        ddF_x += 2;
        f += ddF_x;    
        setPixel(x0 + x, y0 + y);
        setPixel(x0 - x, y0 + y);
        setPixel(x0 + x, y0 - y);
        setPixel(x0 - x, y0 - y);
        setPixel(x0 + y, y0 + x);
        setPixel(x0 - y, y0 + x);
        setPixel(x0 + y, y0 - x);
        setPixel(x0 - y, y0 - x);
    }
}


void ofxPixelMap::drawLine(int x0, int y0, int x1, int y1){
    auto dx = x0-x1;
    auto dy = y0-y1;
    int y=y0;
    for (int x=x0;x<x1;x++){
        y = y1 + (dy) * (x - x1)/(dx);
        setPixel(x, y);
    }
    
}
void ofxPixelMap::eraseLine(int x0, int y0, int x1, int y1,int a){
    auto dx = x0-x1;
    auto dy = y0-y1;
    int y=y0;
    for (int x=x0;x<x1;x++){
        y = y1 + (dy) * (x - x1)/(dx);
        setPixelAlpha(x, y, a);
    }
    
}



void ofxPixelMap::setPixelAlpha(int x, int y,int a){
    if( pixels[((y)*width+x)*4 + 3]<a){
        return;
    }
    pixels[((y%height)*width+x)*4 + 3] =a;
}
void ofxPixelMap::clear(){
    for(int x=0;x<width;x++){
        for(int y=0;y<height;y++){
            pixels[(y*width+x)*4 + 0] = bgColor.r;	// r
            pixels[(y*width+x)*4 + 1] = bgColor.g;	// g
            pixels[(y*width+x)*4 + 2] = bgColor.b;	// b
            pixels[(y*width+x)*4 + 3] = bgColor.a;	// a
        }
    }
}

void drawSnowFlake(int x0,int y0,int a){

}
void ofxPixelMap::saveDefault(){
    for(int x=0;x<width;x++){
        for(int y=0;y<height;y++){
            color.r=pixels[(y*width+x)*4 + 0];
            color.g=pixels[(y*width+x)*4 + 1];	
            color.b=pixels[(y*width+x)*4 + 2];	
            color.a=pixels[(y*width+x)*4 + 3];	
            defaultPixels[(y*width+x)*4 + 0] = bgColor.r;	// r
            defaultPixels[(y*width+x)*4 + 1] = bgColor.g;	// g
            defaultPixels[(y*width+x)*4 + 2] = bgColor.b;	// b
            defaultPixels[(y*width+x)*4 + 3] = bgColor.a;	// a
        }
    }
}
//max of 5 plz
void ofxPixelMap::fadeToDefault(int qty){
    for(int x=0;x<width;x++){
        for(int y=0;y<height;y++){
            if(pixels[(y*width+x)*4 + 3]<250){
                pixels[(y*width+x)*4 + 3]+=qty;	// a
            }
        }
    }
}

void ofxPixelMap::draw(){
    view.draw(0, 0, width, height);
}