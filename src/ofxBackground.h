//
//  ofxBackground.h
//  Particles
//
//  Created by Paul Wand on 6/2/12.
//  Copyright (c) 2012 RGB. All rights reserved.
//

#ifndef Particles_ofxBackground_h
#define Particles_ofxBackground_h
#include "ofMain.h"
enum ofxBGMode{img,vid,live};


class ofxBackground{
public:
    void setup();
    void update();
    void draw();
    void setVideo(string video);
    void setImage(string img);
    void setMode(int mode);
    ofImage defaultImage;
    ofImage *current;
    int mode=0;
//    ofVideoGrabber camera;
    ofVideoPlayer video;

};

#endif
