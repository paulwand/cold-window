

#include "ofxParticleSystem.h"
ofxParticleSystem::ofxParticleSystem() {
}

void ofxParticleSystem::setup(){
    pixels.setup();
    pixels.bgColor.a=0;
    pixels.color.set(200, 200, 255);
    pixels.color.a=100;
    pixels.clear();
    width=ofGetWidth();
    height=ofGetHeight();
    for (int x=0;x<5;x++){
        ofxGravityWell well;
        well.location.set(x, x);
        well.radius=20;
        well.mass=1;
        well.force=3.5;
        well.destination.set(x, x);
        well.id=x;
        wells[well.id]=well;
    }
    for(int x=0;x<5000;x++){
        ofxParticle p;
        inertParticles.push_back(p);
        }
    }


void ofxParticleSystem::update() {
    pixels.clear();
    for (int x=0;x<movingParticles.size();x++) {
        auto particle=&movingParticles[x];
        particle->update();
        pixels.setPixel(((int)particle->loc.x)%width, ((int)particle->loc.y)%height);
       
        for ( auto &well:wells) {
            if(well.active)
                well.gravitate(particle);
        }
     if(particle->life<=0||(particle->vel.x==0 && particle->vel.y==0)){
         particle->life=0;
         ofNotifyEvent(particleDeath,particle->loc,this);
         inertParticles.push_back(*particle);
         movingParticles.erase(movingParticles.begin()+x);
     }
 }
    
    
    pixels.update();
}


//--------------------------------------------------------------
ofxGravityWell* ofxParticleSystem::getGravityWell(int id) {
    return &wells[id];
}
//--------------------------------------------------------------
void ofxParticleSystem::moveGravityWell(int x,int y,int id) {
   wells[id].location.set(x, y);
}

//--------------------------------------------------------------
void ofxParticleSystem::removeGravityWell(int id) {
    wells[id].active = NO;
}

//--------------------------------------------------------------
void ofxParticleSystem::draw() {
    pixels.draw();
}

//--------------------------------------------------------------
void ofxParticleSystem::addParticle(ofxParticle p){
    movingParticles.push_back(p);
    
}

//--------------------------------------------------------------
void ofxParticleSystem::addParticles(int x, int y, int life){

    for (int xx=-10;xx<10;xx+=5){
        for (int yy=-10;yy<10;yy+=5){
            addParticle(x+xx,y+yy,life);
        }
    }
}

void ofxParticleSystem::addParticle(int x,int y,int life){
    if(inertParticles.empty())
        return;
    auto p=inertParticles.front();
    p.loc.x=x;
    p.loc.y=y;
    p.life=life;
    addParticle(p);
    inertParticles.erase(inertParticles.begin());
}

int ofxParticleSystem::numParticles() {
	return movingParticles.size();
}

void ofxParticleSystem::activateGravityWell(int id,int x,int y){
    auto well=getGravityWell(id);
    well->destination.set(x, y);
    well->location.set(x, y);
    well->pLocation.set(x, y);
    well->active=true;
    well->update();

}
