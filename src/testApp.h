#pragma once

#include "ofMain.h"
#include "ofxiPhone.h"
#include "ofxiPhoneExtras.h"
#include "ofxParticleSystem.h"
#include "ofxBackground.h"
#include "ofxColdWindow.h"
#include "ofxBlowDetector.h"
class testApp : public ofxiPhoneApp {
	
public:
	void setup();
	void update();
	void draw();
	void exit();
	
	void touchDown(ofTouchEventArgs &touch);
	void touchMoved(ofTouchEventArgs &touch);
	void touchUp(ofTouchEventArgs &touch);
	void touchDoubleTap(ofTouchEventArgs &touch);
	void touchCancelled(ofTouchEventArgs &touch);

	void lostFocus();
	void gotFocus();
	void gotMemoryWarning();
	void deviceOrientationChanged(int newOrientation);
    int width;
    int height;
    
    #pragma mark audio
    void audioIn(float *input, int bufferSize, int nChannels);

    #pragma mark UI
    void record();
    void share();
    void chooseBackground();
    void chooseForeground();

    #pragma mark particles
    ofxColdWindow window;
//    ofxParticleSystem particles;
//    ofVideoGrabber camera;

};


