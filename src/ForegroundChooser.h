//
//  ForegroundChooser.h
//  ColdWindow
//
//  Created by Paul Wand on 6/14/12.
//  Copyright (c) 2012 RGB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "testApp.h"

@interface ForegroundChooser : UIViewController{
    testApp *myApp;
    UIAlertView *alert;
}


-(IBAction)chooseFrost:(id)sender;
-(IBAction)chooseSnow:(id)sender;
-(IBAction)chooseCold:(id)sender;

@end
