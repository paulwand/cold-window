

#include "ofxParticle.h"

//--------------------------------------------------------------
ofxParticle::ofxParticle() {
	loc.set(1, 1 );
	vel = ofVec2f(1,1);
	accel = ofVec2f(1,1);
	mass = 1.0;
	radius = 1;
    life=50;
}
//--------------------------------------------------------------
void ofxParticle::update() {
    vel += accel;
    prev.set(loc.x, loc.y);
	loc.x += vel.x;
    loc.y += vel.y;
    vel *= friction;
    life -=1;
    accel.set(0, 0);
//    checkEdges();
}
//--------------------------------------------------------------
void ofxParticle::addForce( ofVec2f force) {
	accel += force / mass;
}

//--------------------------------------------------------------
void ofxParticle::draw() {
    ofLine(loc.x, loc.y, loc.x+2, loc.y+2);
}

//--------------------------------------------------------------
void ofxParticle::checkEdges() {    
/*	if (y > height) {
        y=1;
    }
	if (y < 1) {
        y=height;
	}
	if (x > width) {
        x=1;
	}
	if (x < 1) {
        x=width;
	}*/
}