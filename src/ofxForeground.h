//
//  ofxForeground.h
//  ColdWindow
//
//  Created by Paul Wand on 6/26/12.
//  Copyright (c) 2012 RGB. All rights reserved.
//

#ifndef ColdWindow_ofxForeground_h
#define ColdWindow_ofxForeground_h
#include "ofMain.h"
#include "ofxPixelMap.h"
enum CWColdnessType{frost,cold,snow};
enum CWWindowType{plain,carSide,carBack,house};

class ofxForeground{
protected:
    CWColdnessType coldness;
    CWWindowType window;
public:
    unsigned char * scratchPixels;
    ofImage image;
    void setup();
    void update();
    void draw();
    ofxPixelMap pixels;
    void setChillyness(CWColdnessType chillyness);
    void setWindowType(CWWindowType window);
    void reset();
    bool reseting=false;
    void resetStep();
    int resetFrameLength;
    int resetFrame;
};

#endif
