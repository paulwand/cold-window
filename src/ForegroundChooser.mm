//
//  ForegroundChooser.m
//  ColdWindow
//
//  Created by Paul Wand on 6/14/12.
//  Copyright (c) 2012 RGB. All rights reserved.
//

#import "ForegroundChooser.h"
#import "ProgressView.h"


@interface ForegroundChooser ()

@end

@implementation ForegroundChooser

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        myApp=(testApp*)ofGetAppPtr();
     
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}



-(IBAction)chooseFrost:(UIButton*)sender{
    [sender setBackgroundColor:[UIColor blueColor]];
    myApp->window.foreground.setChillyness(frost);
    [self done];
}
-(IBAction)chooseSnow:(UIButton*)sender{
    [sender setBackgroundColor:[UIColor blueColor]];
    myApp->window.foreground.setChillyness(snow);
    [self done];
}
-(IBAction)chooseCold:(UIButton*)sender{

    [sender setBackgroundColor:[UIColor blueColor]];
    
    myApp->window.foreground.setChillyness(cold);
    [self done];
}
-(void)done{
    [UIView transitionWithView:ofxiPhoneGetUIWindow() duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve                     animations:^ { [self.view removeFromSuperview]; }
                    completion:nil];
}

-(void)showProgress{

}


@end
