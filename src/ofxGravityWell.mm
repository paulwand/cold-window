//
//  ofxGravityWell.cpp
//  Particles
//
//  Created by Paul Wand on 5/26/12.
//  Copyright (c) 2012 RGB. All rights reserved.
//

#include "ofxGravityWell.h"

void ofxGravityWell::draw(){
}
void ofxGravityWell::gravitate(ofxParticle *p){
    float xdist=(p->loc.x-location.x);
    float ydist=(p->loc.y-location.y);
    float distance=sqrt(xdist*xdist+ydist*ydist);
    if(distance > radius*.8)
        return;
    frc.x=xdist*(1/distance)*force;
    frc.y=ydist*(1/distance)*force;
    p->addForce(frc);

}
void ofxGravityWell::update(){
    pLocation.set(location.x, location.y);    
}