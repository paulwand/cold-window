//
//  ofxBackground.mm
//  Particles
//
//  Created by Paul Wand on 6/2/12.
//  Copyright (c) 2012 RGB. All rights reserved.
//

#include "ofxBackground.h"

void ofxBackground::setup(){
    defaultImage.loadImage("kittens.jpg");
    current=&defaultImage;
   // camera.initGrabber(ofGetWidth(), ofGetHeight(),OF_PIXELS_BGRA);
    mode=img;
}
void ofxBackground::draw(){
    if(mode==img){
        current->draw(0,0);
    }else if(mode==live){
   //     camera.draw(0, 0, ofGetWidth(), ofGetHeight());
    }else if(mode==vid){
        video.draw(0,0);
    }
}

void ofxBackground::update(){
    if(mode==img){
        current->update();
    }else if(mode==live){
 //       camera.update();
    }else if(mode==vid){
        video.update();
    }
}
void ofxBackground::setMode(int md){
    mode=md;
}
void ofxBackground::setVideo(string vid){
    video.loadMovie(vid);
    video.play();
}
void ofxBackground::setImage(string img){
    current=new ofImage();
    current->loadImage(img);
    current->resize(ofGetWidth(), ofGetHeight());
}
