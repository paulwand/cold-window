//
//  ofxParticleSystem.h
//  Particles
//
//  Created by Paul Wand on 5/24/12.
//  Copyright (c) 2012 RGB. All rights reserved.
//
//  A particle container class
//
//
#pragma once
#include "ofxParticle.h"
#include "ofxGravityWell.h"
#include "ofMath.h"
#include "ofxPixelMap.h"

#import <Accelerate/Accelerate.h>

using namespace std;

class ofxParticleSystem {
public:
    ofxParticleSystem();
    
    
    void setup();
	void update();
	int height,width;
    
    
    ofxGravityWell* getGravityWell(int id);
    void moveGravityWell(int x,int y,int id);
    void removeGravityWell(int id);
    void activateGravityWell(int id,int x,int y);

    void draw();
    
    void addParticle( ofxParticle p);
	void addParticle( int x,int y,int life);
    void addParticles(int x,int y,int life);
    
	int numParticles();
    
	vector<ofxParticle> movingParticles;
	vector<ofxParticle> inertParticles;
    
	ofxGravityWell wells[5];

    ofEvent<ofVec2f> particleDeath;

    ofxPixelMap pixels;
};