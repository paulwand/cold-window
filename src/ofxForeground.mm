//
//  ofxForeground.cpp
//  ColdWindow
//
//  Created by Paul Wand on 6/26/12.
//  Copyright (c) 2012 RGB. All rights reserved.
//


#include "ofxForeground.h"
void ofxForeground::setup(){
    pixels.setup();
}
void ofxForeground::update(){
    if(reseting){
        resetStep();
    }
}
void ofxForeground::draw(){
    pixels.draw();
}

void ofxForeground::setChillyness(CWColdnessType type){    
    switch (type) {
        case cold:
            image.loadImage("cold.jpg");
            resetFrameLength=1000;
            break;
        case frost:
            image.loadImage("frost.jpg");
            resetFrameLength=200;
            break;
        case snow:
            image.loadImage("snow.jpg");
            break;
    }
    image.resize(ofGetWidth(), ofGetHeight());
    //Transfer the photo pixels to the window textures
    scratchPixels=image.getPixels();
    pixels.clear();
    for(int x=0;x<image.width;x++){
        for(int y=0;y<image.height;y++){
            pixels.color.r=scratchPixels[(y*image.width+x)*3 + 0] ;	// r
            pixels.color.g=scratchPixels[(y*image.width+x)*3 + 1] ;	// g
            pixels.color.b=scratchPixels[(y*image.width+x)*3 + 2] ;	// b
            pixels.color.a=255;
            pixels.setPixel(x, y);
        }
    }
    pixels.saveDefault();
    
}

void ofxForeground::setWindowType(CWWindowType window){

};

void ofxForeground::reset(){
    if(!reseting){
        reseting=YES;
        resetFrame=0;
    }
}
void ofxForeground::resetStep(){
   pixels.fadeToDefault(5);
}

