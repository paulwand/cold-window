//
//  ofxColdWindow.h
//  Particles
//
//  Created by Paul Wand on 6/2/12.
//  Copyright (c) 2012 RGB. All rights reserved.
//

#ifndef Particles_ofxColdWindow_h
#define Particles_ofxColdWindow_h

#include "ofxiPhoneExtras.h"
#include "ofMain.h"
#include "ofxPixelMap.h"
#include "ofxGravityWell.h"
#include "ofxParticleSystem.h"
#include "ofxBackground.h"
#include "ofxBlowDetector.h"
#include "ofxForeground.h"

class ofxColdWindow{

public:
    ofImage window;
    ofxForeground foreground;
    ofxBackground background;
    ofxParticleSystem particles;
    void setup();
    void draw();
    void update();
    int touchRadius;
	void touchDown(ofTouchEventArgs &touch);
	void touchMoved(ofTouchEventArgs &touch);
	void touchUp(ofTouchEventArgs &touch);
    string message;
    void particleDeath(ofVec2f & args);
    void blowDetected(ofVec2f &power);
    ofxBlowDetector blowDetector;
    void reset();
    
};
#endif
